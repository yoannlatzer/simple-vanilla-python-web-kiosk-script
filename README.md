# Simple vanilla Python3 Web Kiosk script

A simple script to run a web kiosk on a machine (made to use a Raspberry Pi web kiosk)

This is a simple while loop, opening and killing web browser instances. Simply pass a full screen command to get the "kiosk"-like experience.

## Requirements
Vanilla Python3.

## Getting started

Create a ```data.json``` file in the same folder as app.py

Take a look at ```data.example.json``` to see the variables to fill in.

Run with ```python3 app.py```

## data.json

- ### "os_browser_run_command": **String**
Must be in the format ```browsercommand --option```

Example: ```"chromium-browser --start-fullscreen"```
- ### "urls": **String Array**

Example: ```["https://www.eff.org/", "https://unsplash.com/"]```
- ### "script_max_loop": **Integer**
Set to ```-1``` for infinite looping (running for 15 days on my pi as we speak. any suggestions to enhance performance is welcomed!)

Example: ```10```
- ### "seconds_per_page": **Integer**
The script will sleep for ```x``` seconds before killing the subprocess and running a new browser instance.

Example: ```180```

