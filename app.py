# NO COPYRIGHT WHATSOEVER. 
# Author: Yoann M.E. Latzer - yoannlatzer.fr

import gc
import json
import os
import subprocess
import signal
import time
from datetime import datetime

browser_process = None
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
config_file = open(os.path.join(__location__, 'data.json'))
data = json.load(config_file)

os_browser_run_command = data['os_browser_run_command']
urls = data["urls"]
script_max_loop = data["script_max_loop"]
seconds_per_page = data["seconds_per_page"]

lines = '--------------------'
def print_intro():
    print("""
 .-.-. .-.-. .-.-. .-.-. .-.-. 
( K .'( I .'( O .'( S .'( K .' 
 `.(   `.(   `.(   `.(   `.(                                
    """)


def kill_browser():
    print(lines)
    print('CLOSING BROWSER...')
    print(lines)
    print(datetime.now())
    global browser_process
    os.killpg(os.getpgid(browser_process.pid), signal.SIGTERM)


def visit_url(url):
    print('VISITING:' + url)
    open_browser_cmd = os_browser_run_command + ' ' + url
    global browser_process
    browser_process = subprocess.Popen(open_browser_cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
    time.sleep(seconds_per_page)
    kill_browser()

def run_script(script_max_loop):
    print_intro()
    while script_max_loop != 0:
        for url in urls:
            visit_url(url)
        # Clear unreferenced memory
        gc.collect()
        if script_max_loop != -1:
            script_max_loop -= 1

# Script start
run_script(script_max_loop)